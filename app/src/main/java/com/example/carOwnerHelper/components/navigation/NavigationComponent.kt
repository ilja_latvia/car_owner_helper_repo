package com.example.carOwnerHelper.components.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.carOwnerHelper.composables.*
import com.example.carOwnerHelper.ui.theme.HandsOnKotlinTheme


@Composable
fun NavigationComponent() {
    val navController = rememberNavController()

    HandsOnKotlinTheme {
        NavHost(navController = navController, startDestination = "CarList") {
            composable("CarList") { CarList(navController) }
            composable("CarAdd") { CarAdd(navController) }
            composable("Settings") { Settings(navController) }
            composable("Intervals/{id}") { Intervals(navController, id = it.arguments?.getString("id")) }
            composable("CarDetailed/{id}") { CarDetailed(navController, id = it.arguments?.getString("id")) }
            composable("CarEdit/{id}") { CarEdit(navController, id = it.arguments?.getString("id")) }
            composable("CarDelete/{id}") { CarDelete(navController, id = it.arguments?.getString("id")) }
            composable("Vignettes/{id}") { Vignettes(navController, id = it.arguments?.getString("id")) }
            composable("Insurance/{id}") { Insurance(navController, id = it.arguments?.getString("id")) }
            composable("Inspection/{id}") { Inspection(navController, id = it.arguments?.getString("id")) }
            composable("VignetteEdit/{id}") { VignetteEdit(navController, id = it.arguments?.getString("id")) }
            composable("InsuranceEdit/{id}") { InsuranceEdit(navController, id = it.arguments?.getString("id")) }
            composable("InspectionEdit/{id}") { InspectionEdit(navController, id = it.arguments?.getString("id")) }
        }
    }
}
