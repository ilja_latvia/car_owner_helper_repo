package com.example.carOwnerHelper.database

import android.app.Application
import androidx.lifecycle.*
import androidx.room.Insert
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CarViewModel(application: Application) : AndroidViewModel(application) {

    val readAllData: LiveData<List<Cars>>
    private val repository: MyRepository

    init {
        val myDao = MyDatabase.getInstance(application).myDao()
        repository = MyRepository(myDao)
        readAllData = repository.readAllData
    }

    fun getCarById(id: Int): LiveData<Cars> {
        return repository.getCarById(id)
    }

    fun deleteCarById(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteCarById(id)
        }
    }

    @Insert
    fun addCar(car: Cars) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addCar(car)

        }
    }

    fun updateCar(car: Cars) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateCar(car = car)
        }
    }

    fun deleteCar(car: Cars) {
        viewModelScope.launch (Dispatchers.IO ){
            repository.deleteCar(car = car)
        }
    }

    fun deleteAllCars() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteAllCars()
        }
    }

    //fun createIntervals(inspection: Inspection, insurance: Insurance, vignette: Vignette)
    fun createIntervals() {
        val inspection = Inspection()
        val insurance = Insurance()
        val vignette = Vignette()
        viewModelScope.launch(Dispatchers.IO) {
            repository.createIntervals(inspection, insurance, vignette)
        }
    }
    fun deleteIntervals(id: Int){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteIntervals(id)
        }
    }

    //Inspection
    fun getInspectionById(id: Int): LiveData<Inspection>{
        return repository.getInspectionById(id)
    }

    fun updateInspection(inspection: Inspection) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateInspection(inspection)
        }
    }


    //Insurance
    fun getInsuranceById(id: Int): LiveData<Insurance>{
        return repository.getInsuranceById(id)
    }

    fun updateInsurance(insurance: Insurance) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateInsurance(insurance)
        }
    }


    //Vignette
    fun getVignetteById(id: Int): LiveData<Vignette>{
        return repository.getVignetteById(id)
    }

    fun updateVignette(vignette: Vignette) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateVignette(vignette)
        }
    }





    //getInsuranceById(_id)
    //getVignetteById(_id)
}

class CarViewModelFactory(
    private val application: Application
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        if (modelClass.isAssignableFrom(CarViewModel::class.java)) {
            return CarViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}