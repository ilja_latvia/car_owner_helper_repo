package com.example.carOwnerHelper.database

import android.view.ContextThemeWrapper
import androidx.room.*
import java.util.*


@Entity
data class Inspection(
    @PrimaryKey(autoGenerate = true) val idq: Int = 0,
    val inspectionDate: String = "",
    val inspectionEnd: String = "",
    val result: String = "",
)

@Entity
data class Insurance(
    @PrimaryKey(autoGenerate = true) val idw: Int = 0,
    val insuranceDate: String = "",
    val insuranceEnd: String = "",
    val insurer: String = "",
)

@Entity
data class Vignette(
    @PrimaryKey(autoGenerate = true) val ide: Int = 0,
    val vignetteDate: String = "",
    val vignetteEnd: String = "",
)

@Entity
data class Cars (
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val carMake: String,
    val carModel: String,
    val carNumber: String,
    val isVignetteNeeded: Boolean,
)

@Entity
data class Makes(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val Make: String
)

@Entity
data class Models(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val makeId: Int,
    val Model: String
)

@Entity
data class Settings(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val darkTheme: Boolean,
    val notifications: Boolean,
)
