package com.example.carOwnerHelper.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface MyDatabaseDao {

    //CAR
    @Query("SELECT * from Cars")
    fun getAll(): LiveData<List<Cars>>

    @Query("SELECT * from Cars where Id = :id")
    fun getById(id: Int) : LiveData<Cars>

    @Query("DELETE FROM Cars where Id = :id")
    fun deleteById(id: Int)

    @Insert
    suspend fun insert(item:Cars)

    @Update
    suspend fun update(item:Cars)

    @Delete
    suspend fun delete(item:Cars)

    @Query("DELETE FROM Cars")
    suspend fun deleteAllCars()

    //Intervals
    @Insert
    suspend fun insertInspection(inspection: Inspection)

    @Insert
    suspend fun insertInsurance(insurance: Insurance)

    @Insert
    suspend fun insertVignette(vignette: Vignette)

    @Query("SELECT * from Inspection where Idq = :id")
    fun getInspectionById(id: Int) : LiveData<Inspection>

    @Query("SELECT * from Insurance where Idw = :id")
    fun getInsuranceById(id: Int) : LiveData<Insurance>

    @Query("SELECT * from Vignette where Ide = :id")
    fun getVignetteById(id: Int) : LiveData<Vignette>

    @Update
    suspend fun updateInspection(inspection: Inspection)

    @Update
    suspend fun updateInsurance(insurance: Insurance)

    @Update
    suspend fun updateVignette(vignette: Vignette)

    @Delete
    suspend fun deleteInspection(inspection: Inspection)

    @Delete
    suspend fun deleteInsurance(insurance: Insurance)

    @Delete
    suspend fun deleteVignette(vignette: Vignette)

}