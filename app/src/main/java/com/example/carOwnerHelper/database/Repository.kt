package com.example.carOwnerHelper.database

import androidx.lifecycle.LiveData


class MyRepository(private val myDatabaseDao: MyDatabaseDao) {
    val readAllData : LiveData<List<Cars>> = myDatabaseDao.getAll()
    fun getCarById(id: Int): LiveData<Cars> = myDatabaseDao.getById(id)
    fun deleteCarById(id: Int) = myDatabaseDao.deleteById(id)

    //Car
    suspend fun addCar(car: Cars) {
        myDatabaseDao.insert(car)
    }
    suspend fun updateCar(car: Cars) {
        myDatabaseDao.update(car)
    }
    suspend fun deleteCar(car: Cars) {
        myDatabaseDao.delete(car)
    }
    suspend fun deleteAllCars() {
        myDatabaseDao.deleteAllCars()
    }

    //Intervals
    suspend fun createIntervals(inspection: Inspection, insurance: Insurance, vignette: Vignette){
        myDatabaseDao.insertInspection(inspection)
        myDatabaseDao.insertInsurance(insurance)
        myDatabaseDao.insertVignette(vignette)
    }

    //Inspection
    fun getInspectionById(id :Int): LiveData<Inspection> = myDatabaseDao.getInspectionById(id)

    suspend fun updateInspection(inspection: Inspection) {
        myDatabaseDao.updateInspection(inspection)
    }

    suspend fun deleteIntervals(id: Int) {
        myDatabaseDao.deleteInspection(getInspectionById(id).value ?: Inspection())
        myDatabaseDao.deleteInsurance(getInsuranceById(id).value ?: Insurance())
        myDatabaseDao.deleteVignette(getVignetteById(id).value ?: Vignette())
    }


    //Insurance
    fun getInsuranceById(id: Int): LiveData<Insurance> = myDatabaseDao.getInsuranceById(id)

    suspend fun updateInsurance(insurance: Insurance) {
        myDatabaseDao.updateInsurance(insurance)
    }


    //Vignette
    fun getVignetteById(id: Int): LiveData<Vignette> = myDatabaseDao.getVignetteById(id)

    suspend fun updateVignette(vignette: Vignette) {
        myDatabaseDao.updateVignette(vignette)
    }
}