package com.example.carOwnerHelper

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

val CHANNEL_ID_1 = "TA"
val CHANNEL_ID_2 = "OCTA"
val CHANNEL_ID_3 = "Vignette"

fun createNotificationChannelTA(context: Context) {
    val channelName_1 = "Tehniskās apskates kanāls"
    val channelDescription_1 = "Ieslēdzot šo kanālu tiek sūtīti atgādinājumi par OCTA"
    // Create the NotificationChannel
    val importance = NotificationManager.IMPORTANCE_HIGH
    val mChannel = NotificationChannel(CHANNEL_ID_1, channelName_1, importance)
    mChannel.description = channelDescription_1
    // Register the channel with the system; you can't change the importance
    // or other notification behaviors after this
    val notificationManager: NotificationManager =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    notificationManager.createNotificationChannel(mChannel)
}

fun createNotificationChannelOCTA(context: Context) {
    val channelName_2 = "Apdrošināšanas kanāls"
    val channelDescription_2 = "Ieslēdzot šo kanālu tiek sūtīti atgādinājumi par OCTA"
    // Create the NotificationChannel
    val importance = NotificationManager.IMPORTANCE_HIGH
    val mChannel = NotificationChannel(CHANNEL_ID_2, channelName_2, importance)
    mChannel.description = channelDescription_2
    // Register the channel with the system; you can't change the importance
    // or other notification behaviors after this
    val notificationManager: NotificationManager =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    notificationManager.createNotificationChannel(mChannel)
}

fun createNotificationChannelVignette(context: Context) {
    val channelName_3 = "Vinjetes kanāls"
    val channelDescription_3 = "Ieslēdzot šo kanālu tiek sūtīti atgādinājumi par Vinjeti"
    // Create the NotificationChannel
    val importance = NotificationManager.IMPORTANCE_HIGH
    val mChannel = NotificationChannel(CHANNEL_ID_3, channelName_3, importance)
    mChannel.description = channelDescription_3
    // Register the channel with the system; you can't change the importance
    // or other notification behaviors after this
    val notificationManager: NotificationManager =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    notificationManager.createNotificationChannel(mChannel)
}


fun popTA(context: Context)
{
    var builder = NotificationCompat.Builder(context, CHANNEL_ID_1)
        .setSmallIcon(R.drawable.ic_baseline_directions_car_24)
        .setContentTitle("TA")
        .setContentText("Automašīnai  drīz beigsies Tehniskā Apskate")
        .setPriority(NotificationCompat.PRIORITY_MAX)

    with(NotificationManagerCompat.from(context)) {
        // notificationId is a unique int for each notification that you must define
        val notificationId = 1
        notify(notificationId, builder.build())
    }
}

fun popOCTA(context: Context)
{
    var builder = NotificationCompat.Builder(context, CHANNEL_ID_2)
        .setSmallIcon(R.drawable.ic_baseline_directions_car_24)
        .setContentTitle("OCTA")
        .setContentText("Automašīnai  drīz beigsies OCTA")
        .setPriority(NotificationCompat.PRIORITY_MAX)

    with(NotificationManagerCompat.from(context)) {
        // notificationId is a unique int for each notification that you must define
        val notificationId = 2
        notify(notificationId, builder.build())
    }
}

fun popVignette(context: Context)
{
    var builder = NotificationCompat.Builder(context, CHANNEL_ID_3)
        .setSmallIcon(R.drawable.ic_baseline_directions_car_24)
        .setContentTitle("Vinjete")
        .setContentText("Automašīnai  drīz beigsies Vinjete")
        .setPriority(NotificationCompat.PRIORITY_MAX)

    with(NotificationManagerCompat.from(context)) {
        // notificationId is a unique int for each notification that you must define
        val notificationId = 3
        notify(notificationId, builder.build())
    }
}


