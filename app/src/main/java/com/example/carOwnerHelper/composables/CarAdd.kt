package com.example.carOwnerHelper.composables

import android.app.Application
import android.content.Context
import android.widget.Toast
import androidx.compose.compiler.plugins.kotlin.ComposeFqNames.remember
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.ExtendedFloatingActionButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.RoundedCorner
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.navigate
import com.example.carOwnerHelper.database.Cars
import com.example.carOwnerHelper.database.CarViewModel
import com.example.carOwnerHelper.database.CarViewModelFactory
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import java.util.Date
import java.util.Calendar
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Icon
import androidx.compose.material.Button
import com.example.carOwnerHelper.database.*

val makes = listOf("Audi", "BMW", "Mercedes")
val modelsOfAudi = listOf("A4", "A6", "A8")
val modelsOfBMW = listOf("X3", "X5", "X6")
val modelsOfMercedes = listOf("A-class", "B-class", "C-class")

@Composable
fun CarAdd(navController: NavController) {
    val carinputViewModel = CarinputViewModel()
    val context = LocalContext.current
    val carViewModel: CarViewModel = viewModel(
        factory = CarViewModelFactory(context.applicationContext as Application)
    )
    val carList = carViewModel.readAllData.observeAsState(listOf()).value

    Column {
        CarMakeSelection(carList, carinputViewModel)
        CarModelSelection(carList, carinputViewModel)
        CarNumberInput(carinputViewModel)
        SaveButton(carViewModel, carinputViewModel, navController)
    }
}

@Composable
fun CarMakeSelection(
    carList: List<Cars>,
    carinputViewModel: CarinputViewModel
    ) {
    val carMake: String by carinputViewModel.carMake.observeAsState("")
    var expanded by remember { mutableStateOf(false) }
    if (expanded) carinputViewModel.onInputChangeModel("")
    var selectedText = carMake
    Column {
        OutlinedTextField(
            enabled = false,
            value = selectedText,
            onValueChange = {carinputViewModel.onInputChangeMake(it)},
            modifier = Modifier.fillMaxWidth(),
            label = { Text("Marka") },
            trailingIcon = {
                Icon(Icons.Filled.ArrowDropDown,
                    null,
                    Modifier.clickable { expanded = !expanded })
            }
        )
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier.fillMaxWidth(),
        ) {
            /*carList.forEach { car ->
                DropdownMenuItem(onClick = {
                    selectedText = car.carMake
                }) {
                    Text(text = car.carMake)
                }*/
            for (make in makes) {
                DropdownMenuItem(onClick = {
                    carinputViewModel.onInputChangeMake(make)
                    expanded = false
                }) {
                    Text(text = make)
                }
            }

        }
    }
}

@Composable
fun CarModelSelection(carList: List<Cars>, carinputViewModel: CarinputViewModel) {
    val carModel: String by carinputViewModel.carModel.observeAsState("")
    var expanded by remember { mutableStateOf(false) }
    var selectedText = carModel

    Column {
        OutlinedTextField(
            enabled = false,
            value = selectedText,
            onValueChange = { carinputViewModel.onInputChangeModel(it) },
            modifier = Modifier.fillMaxWidth(),
            label = {Text("Modelis")},
            trailingIcon = {
                Icon(Icons.Filled.ArrowDropDown,"contentDescription", Modifier.clickable { expanded = !expanded })
            }
        )
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier.fillMaxWidth(),
        ) {
            /*carList.forEach { a ->
                DropdownMenuItem(onClick = {
                    selectedText = a.carModel
                }) {
                    Text(text = a.carModel)
                }
            }*/
            var models = listOf<String>()
            when (carinputViewModel.carMake.value.toString()) {
                makes[0] -> models = modelsOfAudi
                makes[1] -> models = modelsOfBMW
                makes[2] -> models = modelsOfMercedes
                else ->
                {
                    carinputViewModel.onInputChangeModel("Sākumā izvēlieties marku")
                    expanded = false
                }
            }
            for (model in models) {
                DropdownMenuItem(onClick = {
                    carinputViewModel.onInputChangeModel(model)
                    expanded = false
                }) {
                    Text(text = model)
                }
            }
        }
    }
}

@Composable
fun CarNumberInput(carinputViewModel: CarinputViewModel){
    val carNumber: String by carinputViewModel.carNumber.observeAsState("")
    OutlinedTextField(
        value = carNumber,
        onValueChange = { carinputViewModel.onInputChangeNumber(it) },
        modifier = Modifier.fillMaxWidth(),
        label = {Text("Reģistrācijas numurs")}
    )
}

@Composable
fun SaveButton(carViewModel: CarViewModel, carinputViewModel: CarinputViewModel, navController: NavController) {
    val context = LocalContext.current
    Button(
        modifier = Modifier.padding(10.dp),
        onClick = {
            if(carinputViewModel.checkData()) {
                insertCarInDB(carViewModel, carinputViewModel)
                navController.navigate("CarList")
            }
            else {
                Toast.makeText(context, "Aizpildiet visus laukus", Toast.LENGTH_SHORT).show()
            }
        }
    )
    {
        Text(text = "Saglabāt")
    }
}
//------------------------------------------------------------------------

class CarinputViewModel : ViewModel() {
    private val _carMake: MutableLiveData<String> = MutableLiveData("")
    private val _carModel: MutableLiveData<String> = MutableLiveData("")
    private val _carNumber: MutableLiveData<String> = MutableLiveData("")
    val carMake: LiveData<String> = _carMake
    val carModel: LiveData<String> = _carModel
    val carNumber: LiveData<String> = _carNumber


    fun onInputChangeMake(newMake: String) {
        _carMake.value = newMake
    }
    fun onInputChangeModel(newModel: String) {
        _carModel.value = newModel
    }
    fun onInputChangeNumber(newNumber: String) {
        _carNumber.value = newNumber.toUpperCase()
    }
    fun checkData(): Boolean
    {
        return !(carMake.value.toString() == "" ||
                carModel.value.toString() == "" ||
                carNumber.value.toString() == "")
    }

}


fun insertCarInDB(carViewModel: CarViewModel, carinputViewModel: CarinputViewModel) {
    val carMake = carinputViewModel.carMake.value.toString()
    val carModel = carinputViewModel.carModel.value.toString()
    val carNumber = carinputViewModel.carNumber.value.toString()
        val cars = Cars(
            carMake = carMake,
            carModel = carModel,
            carNumber = carNumber,
            isVignetteNeeded = false
        )
    carViewModel.createIntervals()
    carViewModel.addCar(cars)

}
