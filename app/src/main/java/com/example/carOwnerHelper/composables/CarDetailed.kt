package com.example.carOwnerHelper.composables

import android.app.Application
import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.TabRowDefaults.Divider
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.Text
import androidx.compose.material.Button
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.SemanticsProperties.Text
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import com.example.carOwnerHelper.database.Cars
import com.example.carOwnerHelper.database.CarViewModel
import com.example.carOwnerHelper.database.CarViewModelFactory

@Composable
fun CarDetailed(navController: NavHostController, id: String?) {

    val context = LocalContext.current
    val carViewModel: CarViewModel = viewModel(
        factory = CarViewModelFactory(context.applicationContext as Application)
    )
    val Cars : Cars? = carViewModel.getCarById(id!!.toInt()).observeAsState().value

    Column {
        TopNavigation(navController, "CarEdit/$id", "Rediģēt")
        TopNavigation(navController, "CarDelete/$id", "Dzēst")

        Text("Marka: ${Cars?.carMake}")
        Text("Modelis: ${Cars?.carModel}")
        Text("Numurs: ${Cars?.carNumber}")
    }
}

