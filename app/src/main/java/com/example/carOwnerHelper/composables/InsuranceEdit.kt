package com.example.carOwnerHelper.composables

import android.app.Application
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.CalendarToday
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.navigate
import com.example.carOwnerHelper.database.*
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.buttons
import com.vanpra.composematerialdialogs.datetime.datepicker.datepicker
import java.time.format.DateTimeFormatter
import com.example.carOwnerHelper.database.CarViewModel
import com.example.carOwnerHelper.database.CarViewModelFactory

@Composable
fun InsuranceEdit(navController: NavController, id: String?) {
    val _id: String = id ?: "-1"
    val insuranceUpdateViewModel = InsuranceUpdateViewModel()
    val context = LocalContext.current
    val carViewModel: CarViewModel = viewModel(
        factory = CarViewModelFactory(context.applicationContext as Application)
    )
    val TA: Insurance? = carViewModel.getInsuranceById(_id.toInt()).observeAsState().value

    Column() {
        StartDateField(insuranceUpdateViewModel, _id)
        EndDateField(insuranceUpdateViewModel, _id)
        InsurerField(insuranceUpdateViewModel, _id)
        UpdateButton(carViewModel, insuranceUpdateViewModel, navController, _id)
    }
}

@Composable
fun StartDateField(insuranceUpdateViewModel: InsuranceUpdateViewModel, id: String) {

    val dialog = MaterialDialog()
    val insuranceDate: String by insuranceUpdateViewModel.insuranceDate.observeAsState("")
    dialog.build {
        datepicker { date ->
            val formattedDate = date.format(
                DateTimeFormatter.ofPattern("dd.MM.yyyy")
            )
            insuranceUpdateViewModel.onInputChange1(formattedDate)
        }
        buttons {
            positiveButton("Saglabāt"){
                println(insuranceDate)
            }
            negativeButton("Atcelt"){
            }
        }
    }
    Box(modifier = Modifier.padding(16.dp)) {
        OutlinedTextField(
            value = insuranceDate,
            onValueChange = {},
            leadingIcon = {
                Icon(Icons.Filled.CalendarToday,"contentDescription", Modifier.clickable { dialog.show() })
            },
            label = {Text("OCTA sākuma datums")}
        )
        Box(
            modifier = Modifier
                .matchParentSize()
                .alpha(0f)
                .clickable(onClick = {
                    dialog.show()
                })
        )
    }
}

@Composable
fun EndDateField(insuranceUpdateViewModel: InsuranceUpdateViewModel, id: String) {
    val dialog = MaterialDialog()
    val endDate: String by insuranceUpdateViewModel.endDate.observeAsState("")
    dialog.build {
        datepicker { date ->
            val formattedDate = date.format(
                DateTimeFormatter.ofPattern("dd.MM.yyyy")
            )
            insuranceUpdateViewModel.onInputChange2(formattedDate)
        }
        buttons {
            positiveButton("Saglabāt"){
                println(endDate)
            }
            negativeButton("Atcelt"){
            }
        }
    }
    Box(modifier = Modifier.padding(16.dp)) {
        OutlinedTextField(
            value = endDate,
            onValueChange = {},
            leadingIcon = {
                Icon(Icons.Filled.CalendarToday,"contentDescription", Modifier.clickable { dialog.show() })
            },
            label = {Text("OCTA beigu datums")}
        )
        Box(
            modifier = Modifier
                .matchParentSize()
                .alpha(0f)
                .clickable(onClick = {
                    dialog.show()
                })
        )
    }
}

@Composable
fun InsurerField( insuranceUpdateViewModel: InsuranceUpdateViewModel, id: String){
    val result: String by insuranceUpdateViewModel.insurer.observeAsState("")
    var expanded by remember { mutableStateOf(false) }
    Column (modifier = Modifier.padding(16.dp)) {
        OutlinedTextField(
            value = result,
            onValueChange = { insuranceUpdateViewModel.onInputChange3(it) },
            label = {Text("Apdrošinātājs")},
        )
    }
}

@Composable
private fun UpdateButton(carViewModel: CarViewModel, insuranceUpdateViewModel: InsuranceUpdateViewModel, navController: NavController, id: String) {
    val context = LocalContext.current
    Button(
        modifier = Modifier.padding(10.dp),
        onClick = {
            if(insuranceUpdateViewModel.checkData()) {
                updateInsuranceInDB(carViewModel, insuranceUpdateViewModel, id)
                Toast.makeText(context, "Updated", Toast.LENGTH_SHORT).show()
                navController.popBackStack()
            }
            else {
                Toast.makeText(context, "Aizpildiet visus laukus", Toast.LENGTH_SHORT).show()
            }
        }
    )
    {
        Text(text = "Saglabāt izmaiņas")
    }
}

//----------------------------

class InsuranceUpdateViewModel : ViewModel() {
    private val _insuranceDate: MutableLiveData<String> = MutableLiveData("")
    private val _endDate: MutableLiveData<String> = MutableLiveData("")
    private val _insurer: MutableLiveData<String> = MutableLiveData("")
    val insuranceDate: LiveData<String> = _insuranceDate
    val endDate: LiveData<String> = _endDate
    val insurer: LiveData<String> = _insurer

    fun onInputChange1(newValue: String) {
        _insuranceDate.value = newValue
    }
    fun onInputChange2(newValue: String) {
        _endDate.value = newValue
    }
    fun onInputChange3(newValue: String) {
        _insurer.value = newValue
    }
    fun checkData(): Boolean
    {
        return !(insuranceDate.value.toString() == "" ||
                endDate.value.toString() == "" ||
                insurer.value.toString() == "")
    }
}

fun updateInsuranceInDB(carViewModel: CarViewModel, insuranceUpdateViewModel: InsuranceUpdateViewModel, id: String) {
    val insuranceDate = insuranceUpdateViewModel.insuranceDate.value.toString()
    val insuranceEnd = insuranceUpdateViewModel.endDate.value.toString()
    val insurer = insuranceUpdateViewModel.insurer.value.toString()
    val insurance = Insurance(
        idw = id.toInt(),
        insuranceDate = insuranceDate,
        insuranceEnd = insuranceEnd,
        insurer = insurer
    )
    carViewModel.updateInsurance(insurance)
}