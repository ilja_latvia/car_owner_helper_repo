package com.example.carOwnerHelper.composables

import android.app.Application
import android.widget.DatePicker
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.navigation.NavHostController
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.navigate
import androidx.room.ColumnInfo
import com.example.carOwnerHelper.database.CarViewModel
import com.example.carOwnerHelper.database.CarViewModelFactory
import com.example.carOwnerHelper.database.Insurance
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.buttons
import com.vanpra.composematerialdialogs.datetime.datepicker.datepicker
import java.time.format.DateTimeFormatter
import java.util.*

@Composable
fun Insurance(navController: NavHostController, id: String?) {

    val _id: Int = id!!.toInt()
    val context = LocalContext.current
    val carViewModel: CarViewModel = viewModel(
        factory = CarViewModelFactory(context.applicationContext as Application)
    )

    val OCTA: Insurance? = carViewModel.getInsuranceById(_id).observeAsState().value

    Column {
        TopNavigation(navController, "InsuranceEdit/$id", "Rediģēt OCTA")

        Column {
            Text("Iegadata: ${OCTA?.insuranceDate ?: "Error"}")
            Text("Deriga lidz: ${OCTA?.insuranceEnd ?: "Error"}")
            Text("Apdrosinatajs: ${OCTA?.insurer ?: "Error"}")
        }
    }
}


