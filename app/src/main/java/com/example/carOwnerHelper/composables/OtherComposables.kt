package com.example.carOwnerHelper.composables

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import com.example.carOwnerHelper.database.CarViewModel

@Composable
fun TopNavigation(
    navController: NavController,
    next: String,
    name: String
) {
    Column {
        Row(
            modifier = Modifier.fillMaxWidth()
        ) {
            Button(
                modifier = Modifier.padding(10.dp),
                onClick = { navController.popBackStack() }
            )
            {
                Text("<- Atpakaļ")
            }
            Button(
                modifier = Modifier.padding(10.dp),
                onClick = { navController.navigate(next)
                }) {
                Text(text = "$name ->")
            }

        }
    }
}