package com.example.carOwnerHelper.composables

import android.app.Application
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import com.example.carOwnerHelper.database.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Composable
fun Intervals(navController: NavHostController, id: String?) {

    val _id: Int = id!!.toInt()
    val context = LocalContext.current
    val carViewModel: CarViewModel = viewModel(
        factory = CarViewModelFactory(context.applicationContext as Application)
    )
    val Cars :Cars? = carViewModel.getCarById(_id).observeAsState().value
    val TA: Inspection? = carViewModel.getInspectionById(_id).observeAsState().value
    val OCTA: Insurance? = carViewModel.getInsuranceById(_id).observeAsState().value
    val vignette: Vignette? = carViewModel.getVignetteById(_id).observeAsState().value

    Column {
        TopNavigation(navController, "CarDetailed/$id", "Informācija par auto")
        Text(text = "Auto marka: ${Cars?.carMake} ->", Modifier.clickable(onClick = {navController.navigate("CarDetailed/$_id")}))
        Text(text = "Auto modelis: ${Cars?.carModel} ->", Modifier.clickable(onClick = {navController.navigate("CarDetailed/$_id")}))

        Spacer(modifier = Modifier.padding(20.dp))
        Text(text = "TA lidz: ${TA?.inspectionEnd ?: "Error"}", Modifier.clickable(onClick = {navController.navigate("Inspection/$_id")}))
        TopNavigation(navController, "Inspection/$id", "Tehniskā apskate")
        Text(text = "OCTA lidz: ${OCTA?.insuranceEnd ?: "Error"}", Modifier.clickable(onClick = {navController.navigate("Insurance/$_id")}))
        TopNavigation(navController, "Insurance/$id", "Apdrošināšana")
        Text(text = "Vinjete lidz: ${vignette?.vignetteEnd ?: "Error"}", Modifier.clickable(onClick = {navController.navigate("Vignettes/$_id")}))
        TopNavigation(navController, "Vignettes/$id", "Viņjete")
    }
}
/*
@Composable
fun Intervals(navController: NavHostController, carId: String?) {
    val _carId: Int = carId!!.toInt()
    Column {
        val car = cars[_carId - 1]
        Text(text = "${car.name} ->", Modifier.clickable(onClick = {navController.navigate("CarDetailed")}))
        Spacer(modifier = Modifier.padding(20.dp))
        Text(text = "TA lidz: dd.mm.gggg ->", Modifier.clickable(onClick = {navController.navigate("Inspection")}))
        Text(text = "OCTA lidz: dd.mm.gggg ->", Modifier.clickable(onClick = {navController.navigate("Insurance")}))
        Text(text = "Vinjete lidz: dd.mm.gggg ->", Modifier.clickable(onClick = {navController.navigate("Vignettes")}))
    }
}
 */