package com.example.carOwnerHelper.composables

import android.app.Application
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.navigate
import com.example.carOwnerHelper.database.Cars
import com.example.carOwnerHelper.database.CarViewModel
import com.example.carOwnerHelper.database.CarViewModelFactory
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.runtime.setValue
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Icon
import androidx.compose.material.Button
import com.example.carOwnerHelper.database.*

@Composable
fun CarEdit(navController: NavController, id: String?) {
    val _id: String = id ?: "-1"
    val carEditViewModel = CarEditViewModel()
    val context = LocalContext.current
    val carViewModel: CarViewModel = viewModel(
        factory = CarViewModelFactory(context.applicationContext as Application)
    )
    val Car : Cars? = carViewModel.getCarById(id!!.toInt()).observeAsState().value
    val carMakeOld = Car?.carMake ?: "Not set"
    val carModelOld = Car?.carModel ?: "Not set"
    val carNumberOld = Car?.carNumber ?: "Not set"

    Column {
        CarMakeEdit(carMakeOld, carEditViewModel)
        CarModelEdit(carModelOld, carEditViewModel)
        CarNumberEdit(carNumberOld, carEditViewModel)
        CarEditButton(carViewModel, carEditViewModel, navController, _id)
    }
}

@Composable
fun CarMakeEdit(
    carMakeOld: String,
    carEditViewModel: CarEditViewModel
) {
    val carMake: String by carEditViewModel.carMake.observeAsState("")
    var expanded by remember { mutableStateOf(false) }
    if (expanded) carEditViewModel.onInputChangeModel("")

    Column {
        OutlinedTextField(
            enabled = false,
            value = carMake,
            onValueChange = {carEditViewModel.onInputChangeMake(it)},
            modifier = Modifier.fillMaxWidth(),
            label = { Text("Marka") },
            trailingIcon = {
                Icon(Icons.Filled.ArrowDropDown,
                    null,
                    Modifier.clickable { expanded = !expanded })
                }
        )
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier.fillMaxWidth(),
        ) {
            for (make in makes) {
                DropdownMenuItem(onClick = {
                    carEditViewModel.onInputChangeMake(make)
                    expanded = false
                }) {
                    Text(text = make)
                }
            }
        }
    }
}

@Composable
fun CarModelEdit(carModelOld: String, carEditViewModel: CarEditViewModel) {
    val carModel: String by carEditViewModel.carModel.observeAsState("")
    var expanded by remember { mutableStateOf(false) }
    var selectedText = carModel

    Column {
        OutlinedTextField(
            enabled = false,
            value = selectedText,
            onValueChange = { carEditViewModel.onInputChangeModel(it) },
            modifier = Modifier.fillMaxWidth(),
            label = {Text("Modelis")},
            trailingIcon = {
                Icon(Icons.Filled.ArrowDropDown,"contentDescription", Modifier.clickable { expanded = !expanded })
            }
        )
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier.fillMaxWidth(),
        ) {
            /*carList.forEach { a ->
                DropdownMenuItem(onClick = {
                    selectedText = a.carModel
                }) {
                    Text(text = a.carModel)
                }
            }*/
            var models = listOf<String>()
            when (carEditViewModel.carMake.value.toString()) {
                makes[0] -> models = modelsOfAudi
                makes[1] -> models = modelsOfBMW
                makes[2] -> models = modelsOfMercedes
                else ->
                {
                    carEditViewModel.onInputChangeModel("Sākumā izvēlieties marku")
                    expanded = false
                }
            }
            for (model in models) {
                DropdownMenuItem(onClick = {
                    carEditViewModel.onInputChangeModel(model)
                    expanded = false
                }) {
                    Text(text = model)
                }
            }
        }
    }
}

@Composable
fun CarNumberEdit(carNumberOld: String, carEditViewModel: CarEditViewModel){
    val carNumber: String by carEditViewModel.carNumber.observeAsState("")
    OutlinedTextField(
        value = carNumber,
        onValueChange = { carEditViewModel.onInputChangeNumber(it) },
        modifier = Modifier.fillMaxWidth(),
        label = {Text("Reģistrācijas numurs")}
    )
}

@Composable
fun CarEditButton(carViewModel: CarViewModel, carEditViewModel: CarEditViewModel, navController: NavController, _id: String) {
    val context = LocalContext.current
    Button(
        modifier = Modifier.padding(10.dp),
        onClick = {
            if(carEditViewModel.checkData()) {
                editCarInDB(carViewModel, carEditViewModel, _id)
                Toast.makeText(context, "Updated", Toast.LENGTH_LONG).show()
                navController.popBackStack()
            }
            else {
                Toast.makeText(context, "Aizpildiet visus laukus", Toast.LENGTH_SHORT).show()
            }
        }
    )
    {
        Text(text = "Saglabāt izmaiņas")
    }
}
//------------------------------------------------------------------------

class CarEditViewModel : ViewModel() {
    private val _carMake: MutableLiveData<String> = MutableLiveData("")
    private val _carModel: MutableLiveData<String> = MutableLiveData("")
    private val _carNumber: MutableLiveData<String> = MutableLiveData("")
    val carMake: LiveData<String> = _carMake
    val carModel: LiveData<String> = _carModel
    val carNumber: LiveData<String> = _carNumber


    fun onInputChangeMake(newMake: String) {
        _carMake.value = newMake
    }
    fun setInitialMake(oldMake: String){
        _carMake.value = oldMake
    }

    fun onInputChangeModel(newModel: String) {
        _carModel.value = newModel
    }
    fun onInputChangeNumber(newNumber: String) {
        _carNumber.value = newNumber.toUpperCase()
    }
    fun checkData(): Boolean
    {
        return !(carMake.value.toString() == "" ||
                carModel.value.toString() == "" ||
                carNumber.value.toString() == "")
    }

}


fun editCarInDB(carViewModel: CarViewModel, carEditViewModel: CarEditViewModel, _id: String) {
    val carMake = carEditViewModel.carMake.value.toString()
    val carModel = carEditViewModel.carModel.value.toString()
    val carNumber = carEditViewModel.carNumber.value.toString()
    val car = Cars(
        id = _id.toInt(),
        carMake = carMake,
        carModel = carModel,
        carNumber = carNumber,
        isVignetteNeeded = false
    )
    carViewModel.updateCar(car)
}
