package com.example.carOwnerHelper.composables

import android.app.Application
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.CalendarToday
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.navigate
import com.example.carOwnerHelper.database.*
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.buttons
import com.vanpra.composematerialdialogs.datetime.datepicker.datepicker
import java.time.format.DateTimeFormatter
import com.example.carOwnerHelper.database.CarViewModel
import com.example.carOwnerHelper.database.CarViewModelFactory

@Composable
fun InspectionEdit(navController: NavController, id: String?) {
    val _id: String = id ?: "-1"
    val inspectionUpdateViewModel = InspectionUpdateViewModel()
    val context = LocalContext.current
    val carViewModel: CarViewModel = viewModel(
        factory = CarViewModelFactory(context.applicationContext as Application)
    )
    val TA: Inspection? = carViewModel.getInspectionById(_id.toInt()).observeAsState().value

    Column() {
        StartDateField(inspectionUpdateViewModel, _id)
        EndDateField(inspectionUpdateViewModel, _id)
        ResultField(inspectionUpdateViewModel, _id)
        UpdateButton(carViewModel, inspectionUpdateViewModel, navController, _id)
    }
}

@Composable
fun StartDateField(inspectionUpdateViewModel: InspectionUpdateViewModel, id: String) {

    val dialog = MaterialDialog()
    val inspectionDate: String by inspectionUpdateViewModel.inspectionDate.observeAsState("")
    dialog.build {
        datepicker { date ->
            val formattedDate = date.format(
                DateTimeFormatter.ofPattern("dd.MM.yyyy")
            )
            inspectionUpdateViewModel.onInputChange1(formattedDate)
        }
        buttons {
            positiveButton("Saglabāt"){
                println(inspectionDate)
            }
            negativeButton("Atcelt"){
            }
        }
    }
    Box(modifier = Modifier.padding(16.dp)) {
        OutlinedTextField(
            value = inspectionDate,
            onValueChange = {},
            leadingIcon = {
                Icon(Icons.Filled.CalendarToday,"contentDescription", Modifier.clickable { dialog.show() })
            },
            label = {Text("TA veikšanas datums")}
        )
        Box(
            modifier = Modifier
                .matchParentSize()
                .alpha(0f)
                .clickable(onClick = {
                    dialog.show()
                })
        )
    }
}

@Composable
fun EndDateField(inspectionUpdateViewModel: InspectionUpdateViewModel, id: String) {
    val dialog = MaterialDialog()
    val endDate: String by inspectionUpdateViewModel.endDate.observeAsState("")
    dialog.build {
        datepicker { date ->
            val formattedDate = date.format(
                DateTimeFormatter.ofPattern("dd.MM.yyyy")
            )
            inspectionUpdateViewModel.onInputChange2(formattedDate)
        }
        buttons {
            positiveButton("Saglabāt"){
                println(endDate)
            }
            negativeButton("Atcelt"){
            }
        }
    }
    Box(modifier = Modifier.padding(16.dp)) {
        OutlinedTextField(
            value = endDate,
            onValueChange = {},
            leadingIcon = {
                Icon(Icons.Filled.CalendarToday,"contentDescription", Modifier.clickable { dialog.show() })
            },
            label = {Text("TA derīga līdz")}
        )
        Box(
            modifier = Modifier
                .matchParentSize()
                .alpha(0f)
                .clickable(onClick = {
                    dialog.show()
            })
        )
    }
}

@Composable
fun ResultField( inspectionUpdateViewModel: InspectionUpdateViewModel, id: String){
    val result: String by inspectionUpdateViewModel.result.observeAsState("")
    var expanded by remember { mutableStateOf(false) }
    Column (modifier = Modifier.padding(16.dp)) {
        OutlinedTextField(
            enabled = false,
            value = result,
            onValueChange = { inspectionUpdateViewModel.onInputChange3(it) },
            label = {Text("TA vērtējums")},
            trailingIcon = {
                Icon(Icons.Filled.ArrowDropDown,"contentDescription", Modifier.clickable { expanded = !expanded })
            }
        )
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier.fillMaxWidth(),
        ) {
            val results = listOf(0, 1, 2, 3)
            for (result in results) {
                DropdownMenuItem(onClick = {
                    inspectionUpdateViewModel.onInputChange3(result.toString())
                    expanded = false
                }) {
                    Text(text = result.toString())
                }
            }
        }
    }
}

@Composable
private fun UpdateButton(carViewModel: CarViewModel, inspectionUpdateViewModel: InspectionUpdateViewModel, navController: NavController, id: String) {
    val context = LocalContext.current
    Button(
        modifier = Modifier.padding(10.dp),
        onClick = {
            if(inspectionUpdateViewModel.checkData()) {
                updateInspectionInDB(carViewModel, inspectionUpdateViewModel, id)
                Toast.makeText(context, "Updated", Toast.LENGTH_SHORT).show()
                navController.popBackStack()
            }
            else {
                Toast.makeText(context, "Aizpildiet visus laukus", Toast.LENGTH_SHORT).show()
            }
        }
    )
    {
        Text(text = "Saglabāt izmaiņas")
    }
}

//----------------------------

class InspectionUpdateViewModel : ViewModel() {
    private val _inspectionDate: MutableLiveData<String> = MutableLiveData("")
    private val _endDate: MutableLiveData<String> = MutableLiveData("")
    private val _result: MutableLiveData<String> = MutableLiveData("")
    val inspectionDate: LiveData<String> = _inspectionDate
    val endDate: LiveData<String> = _endDate
    val result: LiveData<String> = _result

    fun onInputChange1(newValue: String) {
        _inspectionDate.value = newValue
    }
    fun onInputChange2(newValue: String) {
        _endDate.value = newValue
    }
    fun onInputChange3(newValue: String) {
        _result.value = newValue
    }
    fun checkData(): Boolean
    {
        return !(inspectionDate.value.toString() == "" ||
                endDate.value.toString() == "" ||
                result.value.toString() == "")
    }
}

fun updateInspectionInDB(carViewModel: CarViewModel, inspectionUpdateViewModel: InspectionUpdateViewModel, id: String) {
    val inspectionDate = inspectionUpdateViewModel.inspectionDate.value.toString()
    val inspectionEnd = inspectionUpdateViewModel.endDate.value.toString()
    val result = inspectionUpdateViewModel.result.value.toString()
    val inspection = Inspection(
        idq = id.toInt(),
        inspectionDate = inspectionDate,
        inspectionEnd = inspectionEnd,
        result = result
    )
    carViewModel.updateInspection(inspection)
}