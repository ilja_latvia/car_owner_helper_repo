package com.example.carOwnerHelper.composables

import android.app.Application
import android.widget.Toast
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import com.example.carOwnerHelper.database.CarViewModel
import com.example.carOwnerHelper.database.CarViewModelFactory

@Composable
fun CarDelete(navController: NavHostController, id: String?) {
    val context = LocalContext.current
    val carViewModel: CarViewModel = viewModel(
        factory = CarViewModelFactory(context.applicationContext as Application)
    )
    Button(onClick = {
        carViewModel.deleteCarById(id!!.toInt())
        carViewModel.deleteIntervals(id!!.toInt())
        Toast.makeText(context, "Car deleted", Toast.LENGTH_SHORT).show()
        navController.navigate("CarList")
    }) {
        Text(text = "Nospiediet lai izdzēstu automobili")
    }
}