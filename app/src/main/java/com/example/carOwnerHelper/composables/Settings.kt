package com.example.carOwnerHelper.composables

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Switch
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.carOwnerHelper.*

@Composable
fun Settings(navController: NavController) {
    val context = LocalContext.current

    Column (modifier = Modifier.padding(16.dp))
    {
        Button(onClick = { popTA(context) }) {
            Text("Palaist atgādinājumu par Tehnisko Apskati")
        }
        Button(onClick = { popOCTA(context) }) {
            Text("Palaist atgādinājumu par Apdrošināšanu")
        }
        Button(onClick = { popVignette(context) }) {
            Text("Palaist atgādinājumu par Vinjeti")
        }
        Row(){
            Text(text = "Atgādinājumi")
            SwitchComponent()
        }
        Row(){
            Text(text = "Tumšā tēma")
            SwitchComponent()
        }
    }
}

@Composable
fun SwitchComponent() {
    var check by remember { mutableStateOf(false) }
    Switch(
        checked = check,
        onCheckedChange = { checked ->
            check = !check
        }
    )
}