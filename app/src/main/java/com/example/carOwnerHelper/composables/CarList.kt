package com.example.carOwnerHelper.composables

import android.app.Application
import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ListItem
import androidx.compose.material.Text
import androidx.compose.material.Button
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.navigate
import com.example.carOwnerHelper.database.Cars
import com.example.carOwnerHelper.database.CarViewModel
import com.example.carOwnerHelper.database.CarViewModelFactory
import androidx.compose.foundation.lazy.items
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.Checkbox
import androidx.compose.material.TabRowDefaults.Divider
import androidx.compose.material.Icon
import androidx.compose.material.icons.filled.Delete
import androidx.compose.ui.graphics.Color
import com.example.carOwnerHelper.components.navigation.NavigationComponent
import androidx.compose.material.AlertDialog

@Composable
fun CarList(navController: NavController) {
    val context = LocalContext.current
    val carViewModel: CarViewModel = viewModel(
        factory = CarViewModelFactory(context.applicationContext as Application)
    )

    val items = carViewModel.readAllData.observeAsState(listOf()).value

    Column(
        modifier = Modifier.padding(16.dp)
    ) {
        Text("Automašīnu saraksts", modifier = Modifier.padding(top = 32.dp))
        Spacer(modifier = Modifier.padding(bottom = 16.dp))
        CustomCardState(navController, carViewModel)
        CarList(list = items, navController)
        Spacer(modifier = Modifier.padding(top = 32.dp))
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun CarList(
    list: List<Cars>,
    navController: NavController,
    ) {
    LazyColumn() {
        items(list) { car ->
            ListItem(
                text = { Text(text = car.carMake + " " + car.carModel) },
                modifier = Modifier.clickable { navController.navigate("Intervals/${car.id}") }
            )
            Divider()
        }
    }
}

@Composable
fun CustomCardState(
    navController: NavController,
    carViewModel: CarViewModel
) {
    Column {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
        ) {
            Button(onClick = { navController.navigate("CarAdd") }) {
                Text(text = "Pievienot")
            }
            Button(onClick = { navController.navigate("Settings") }) {
                Text(text = "Iestatījumi")
            }
            Button(onClick = { carViewModel.deleteAllCars() }) {
                Text(text = "Izdzēst visus")
            }
        }
    }
}
