package com.example.carOwnerHelper.composables

import android.app.Application
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.CalendarToday
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.navigate
import com.example.carOwnerHelper.database.*
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.buttons
import com.vanpra.composematerialdialogs.datetime.datepicker.datepicker
import java.time.format.DateTimeFormatter
import com.example.carOwnerHelper.database.CarViewModel
import com.example.carOwnerHelper.database.CarViewModelFactory

@Composable
fun VignetteEdit(navController: NavController, id: String?) {
    val _id: String = id ?: "-1"
    val vignetteUpdateViewModel = VignetteUpdateViewModel()
    val context = LocalContext.current
    val carViewModel: CarViewModel = viewModel(
        factory = CarViewModelFactory(context.applicationContext as Application)
    )
    val TA: Vignette? = carViewModel.getVignetteById(_id.toInt()).observeAsState().value

    Column() {
        StartDateField(vignetteUpdateViewModel, _id)
        EndDateField(vignetteUpdateViewModel, _id)
        UpdateButton(carViewModel, vignetteUpdateViewModel, navController, _id)
    }
}

@Composable
fun StartDateField(vignetteUpdateViewModel: VignetteUpdateViewModel, id: String) {

    val dialog = MaterialDialog()
    val vignetteDate: String by vignetteUpdateViewModel.vignetteDate.observeAsState("")
    dialog.build {
        datepicker { date ->
            val formattedDate = date.format(
                DateTimeFormatter.ofPattern("dd.MM.yyyy")
            )
            vignetteUpdateViewModel.onInputChange1(formattedDate)
        }
        buttons {
            positiveButton("Saglabāt"){
                println(vignetteDate)
            }
            negativeButton("Atcelt"){
            }
        }
    }
    Box(modifier = Modifier.padding(16.dp)) {
        OutlinedTextField(
            value = vignetteDate,
            onValueChange = {},
            leadingIcon = {
                Icon(Icons.Filled.CalendarToday,"contentDescription", Modifier.clickable { dialog.show() })
            },
            label = {Text("Vignetes sākuma datums")}
        )
        Box(
            modifier = Modifier
                .matchParentSize()
                .alpha(0f)
                .clickable(onClick = {
                    dialog.show()
                })
        )
    }
}

@Composable
fun EndDateField(vignetteUpdateViewModel: VignetteUpdateViewModel, id: String) {
    val dialog = MaterialDialog()
    val endDate: String by vignetteUpdateViewModel.endDate.observeAsState("")
    dialog.build {
        datepicker { date ->
            val formattedDate = date.format(
                DateTimeFormatter.ofPattern("dd.MM.yyyy")
            )
            vignetteUpdateViewModel.onInputChange2(formattedDate)
        }
        buttons {
            positiveButton("Saglabāt"){
                println(endDate)
            }
            negativeButton("Atcelt"){
            }
        }
    }
    Box(modifier = Modifier.padding(16.dp)) {
        OutlinedTextField(
            value = endDate,
            onValueChange = {},
            leadingIcon = {
                Icon(Icons.Filled.CalendarToday,"contentDescription", Modifier.clickable { dialog.show() })
            },
            label = {Text("Vinjetes beigu datums")}
        )
        Box(
            modifier = Modifier
                .matchParentSize()
                .alpha(0f)
                .clickable(onClick = {
                    dialog.show()
                })
        )
    }
}


@Composable
private fun UpdateButton(carViewModel: CarViewModel, vignetteUpdateViewModel: VignetteUpdateViewModel, navController: NavController, id: String) {
    val context = LocalContext.current
    Button(
        modifier = Modifier.padding(10.dp),
        onClick = {
            if(vignetteUpdateViewModel.checkData()) {
                updateVignetteInDB(carViewModel, vignetteUpdateViewModel, id)
                Toast.makeText(context, "Updated", Toast.LENGTH_SHORT).show()
                navController.popBackStack()
            }
            else {
                Toast.makeText(context, "Aizpildiet visus laukus", Toast.LENGTH_SHORT).show()
            }
        }
    )
    {
        Text(text = "Saglabāt izmaiņas")
    }
}

//----------------------------

class VignetteUpdateViewModel : ViewModel() {
    private val _vignetteDate: MutableLiveData<String> = MutableLiveData("")
    private val _endDate: MutableLiveData<String> = MutableLiveData("")
    private val _result: MutableLiveData<String> = MutableLiveData("")
    val vignetteDate: LiveData<String> = _vignetteDate
    val endDate: LiveData<String> = _endDate

    fun onInputChange1(newValue: String) {
        _vignetteDate.value = newValue
    }

    fun onInputChange2(newValue: String) {
        _endDate.value = newValue
    }

    fun checkData(): Boolean
    {
        return !(vignetteDate.value.toString() == "" ||
                endDate.value.toString() == "")
    }
}

fun updateVignetteInDB(carViewModel: CarViewModel, vignetteUpdateViewModel: VignetteUpdateViewModel, id: String) {
    val vignetteDate = vignetteUpdateViewModel.vignetteDate.value.toString()
    val vignetteEnd = vignetteUpdateViewModel.endDate.value.toString()
    val vignette = Vignette(
        ide = id.toInt(),
        vignetteDate = vignetteDate,
        vignetteEnd = vignetteEnd,
    )
    carViewModel.updateVignette(vignette)
}