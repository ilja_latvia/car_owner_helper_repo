package com.example.carOwnerHelper

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import com.example.carOwnerHelper.components.navigation.NavigationComponent

class MainActivity : AppCompatActivity() {
    var selectedCarId = "-1"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NavigationComponent()
        }

        createNotificationChannelTA(this)
        createNotificationChannelOCTA(this)
        createNotificationChannelVignette(this)

    }
}

